require 'watir'

browser = Watir::Browser.new :chrome, options: { headless: true }

browser.goto 'google.com'
browser.text_field(name: 'q').when_present.set('software automated testing')
browser.button(:class => 'gNO89b').exists?
browser.button(:class => 'gNO89b').click
browser.h3s(:class=> 'LC20lb')[0].click

sleep 5
browser.close
